package oardatabase

import (
	"database/sql"
	"fmt"
	"time"

	// Postgres driver for database/sql
	_ "github.com/lib/pq"
	"github.com/prometheus/common/log"
)

// OARDatabase OAR database struct
type OARDatabase struct {
	db       *sql.DB
	Hostname string
	Port     string
	User     string
	Password string
	DBName   string
}

const (
	getJobsCountSQL = `SELECT
	(SELECT COUNT(*) FROM jobs WHERE state='Running') as running,
	(SELECT COUNT(*) FROM jobs WHERE state='Waiting') as waiting,
	(SELECT COUNT(*) FROM jobs WHERE state='Finishing') as finishing
	`
	getQueuesSQL             = "SELECT queue_name,priority,state FROM queues"
	getRunningJobByQueuesSQL = "SELECT queue_name, COUNT(*) FROM jobs WHERE state='Running' GROUP BY queue_name"
	getWaitingJobByQueuesSQL = "SELECT queue_name, COUNT(*) FROM jobs WHERE state='Waiting' GROUP BY queue_name"
	getNodesStatesSQL        = `SELECT
	(SELECT COUNT(*) FROM resources WHERE state='Alive') as alive,
	(SELECT COUNT(*) FROM resources WHERE state='Dead') as dead,
	(SELECT COUNT(*) FROM resources WHERE state='Suspected') as suspected
	`
	getSuspectedNodes = `SELECT COUNT(DISTINCT network_address) FROM resources WHERE state='Suspected' AND network_address NOT IN
	(SELECT distinct(network_address) FROM resources WHERE resource_id IN 
	(SELECT resource_id FROM assigned_resources WHERE assigned_resource_index = 'CURRENT'))
	`
)

// Queue Oar queue structure
type Queue struct {
	Name     string
	Priority int32
	State    string
}

// Node OAR node structure
type Node struct {
	Host  string
	State string
}

func (d *OARDatabase) connectionString() string {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", d.Hostname, d.Port, d.User, d.Password, d.DBName)
	return psqlInfo
}

// InitConnection init OAR database connection
func (d *OARDatabase) InitConnection() error {
	if d.db != nil {
		return nil
	}

	var conn *sql.DB
	conn, err := sql.Open("postgres", d.connectionString())
	if err == nil {
		err = conn.Ping()
	}
	if err != nil {
		return err
	}

	d.db = conn
	return nil
}

// CloseDatabase Close OAR database connection
func (d *OARDatabase) CloseDatabase() error {
	if d.db != nil {
		err := d.db.Close()
		if err != nil {
			return err
		}
		d.db = nil
	}

	return nil
}

func (d *OARDatabase) sqlQuery(query string, iteration func(r *sql.Rows) error) (err error) {
	if err := d.db.Ping(); err != nil {
		for {
			fmt.Println("Reconnecting to posqtgresql server...")
			d.CloseDatabase()
			if err := d.InitConnection(); err != nil {
				fmt.Println("Failed reconnect to postgresql server, retrying in 10s.")
			} else {
				fmt.Println("Connected to postgresql database.")
				break
			}
			time.Sleep(10 * time.Second)
		}

	}

	var rows *sql.Rows
	if rows, err = d.db.Query(query); err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		iteration(rows)
	}
	return nil
}

// GetJobsCounts Get jobs counts by states
func (d *OARDatabase) GetJobsCounts() (counts map[string]int32, err error) {
	m := make(map[string]int32)
	if err = d.sqlQuery(getJobsCountSQL, func(r *sql.Rows) error {
		var running, waiting, finishing int32
		if err := r.Scan(&running, &waiting, &finishing); err != nil {
			log.Error(err, "")
			return err
		}
		m["running"] = running
		m["waiting"] = waiting
		m["finishing"] = finishing
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}

	counts = m
	return counts, nil
}

// GetQueues Get OAR queues
func (d *OARDatabase) GetQueues() (queues []Queue, err error) {
	if err = d.sqlQuery(getQueuesSQL, func(r *sql.Rows) error {
		var name, state string
		var priority int32
		if err := r.Scan(&name, &priority, &state); err != nil {
			log.Error(err, "")
			return err
		}
		queues = append(queues, Queue{Name: name, Priority: priority, State: state})
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}
	return queues, nil
}

// GetRunningJobsByQueues Get OAR running jobs by queues
func (d *OARDatabase) GetRunningJobsByQueues() (counts map[string]int32, err error) {
	m := make(map[string]int32)
	if err = d.sqlQuery(getRunningJobByQueuesSQL, func(r *sql.Rows) error {
		var queue string
		var count int32
		if err := r.Scan(&queue, &count); err != nil {
			log.Error(err, "")
			return err
		}
		m[queue] = count
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}
	counts = m
	return counts, nil
}

// GetWaitingJobsByQueues Get OAR waiting jobs by queues
func (d *OARDatabase) GetWaitingJobsByQueues() (counts map[string]int32, err error) {
	m := make(map[string]int32)
	if err = d.sqlQuery(getWaitingJobByQueuesSQL, func(r *sql.Rows) error {
		var queue string
		var count int32
		if err := r.Scan(&queue, &count); err != nil {
			log.Error(err, "")
			return err
		}
		m[queue] = count
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}
	counts = m
	return counts, nil
}

// GetNodesState Get OAR nodes state
func (d *OARDatabase) GetNodesState() (nodes []Node, err error) {
	if err = d.sqlQuery(getNodesStatesSQL, func(r *sql.Rows) error {
		var host, state string
		if err := r.Scan(&host, &state); err != nil {
			log.Error(err, "")
			return err
		}
		nodes = append(nodes, Node{Host: host, State: state})
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}
	return nodes, nil
}

// GetResourceCountByState Count OAR resources by state
func (d *OARDatabase) GetResourceCountByState() (counts map[string]int32, err error) {
	m := make(map[string]int32)
	if err = d.sqlQuery(getNodesStatesSQL, func(r *sql.Rows) error {
		var alive, dead, suspected int32
		if err := r.Scan(&alive, &dead, &suspected); err != nil {
			log.Error(err, "")
			return err
		}
		m["alive"] = alive
		m["dead"] = dead
		m["suspected"] = suspected
		return nil
	}); err != nil {
		log.Error(err, "")
		return nil, err
	}
	counts = m
	return counts, nil
}

// GetSuspectedNodes Count OAR suspected nodes without any jobs (ready to restart)
func (d *OARDatabase) GetSuspectedNodes() (counts int32, err error) {
	var count int32
	if err = d.sqlQuery(getSuspectedNodes, func(r *sql.Rows) error {
		if err := r.Scan(&count); err != nil {
			log.Error(err, "")
			return err
		}
		return nil
	}); err != nil {
		log.Error(err, "")
		return count, err
	}
	return count, nil
}
