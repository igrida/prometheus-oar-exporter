PLATFORMS := linux/amd64 darwin/amd64
DOCKER_IMAGE_TAG ?= $(subst release/,v,$(shell git rev-parse --abbrev-ref HEAD))

temp = $(subst /, ,$@)
os = $(word 1, $(temp))
arch = $(word 2, $(temp))

release: $(PLATFORMS)

$(PLATFORMS):
	@echo === go build prometheus-oar-exporter for $(os)-$(arch)
	[ -d ./bin ] || mkdir bin
	GOOS=$(os) GOARCH=$(arch) go build -o './bin/prometheus-oar-exporter-$(os)-$(arch)' main.go

clean:
	rm -f ./bin/*

docker.build: linux/amd64
	@echo === Docker build
	docker build --rm -f ./Dockerfile -t registry.gitlab.inria.fr/igrida/kubigrida-docker/prometheus-oar-exporter:$(DOCKER_IMAGE_TAG) -t registry.gitlab.inria.fr/igrida/kubigrida-docker/prometheus-oar-exporter:latest ./

.PHONY: release $(PLATFORMS) clean