# Build the manager binary
FROM golang:1.19 as builder

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY oardatabase/ oardatabase/

ARG ARCH="amd64"
ARG OS="linux"

# Build
RUN CGO_ENABLED=0 GOOS=${OS} GOARCH=${ARCH} GO111MODULE=on go build -a -o prometheus-oar-exporter main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
#FROM gcr.io/distroless/static:nonroot
FROM alpine:3.13
WORKDIR /
COPY --from=builder /workspace/prometheus-oar-exporter ./bin
#USER 65532:65532

EXPOSE      9103
ENTRYPOINT ["/bin/prometheus-oar-exporter"]