package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"gitlab.inria.fr/igrida/prometheus-oar-exporter/oardatabase"
)

var (
	exporterAddr        = flag.String("listen-address", ":9104", "The address to listen on for HTTP requests.")
	oarPostgresHostname = flag.String("oar-postgres-hostname", "oar-postgres", "OAR Posgresql server hostname.")
	oarPostgresPort     = flag.String("oar-postgres-port", "5432", "OAR Postgresql server port.")
	oarPostgresUser     = flag.String("oar-postgres-user", "oarreader", "OAR Postgresql server user.")
	oarPostgresPassword = flag.String("oar-postgres-password", "read", "OAR Postgresql server password.")
	oarPostgresDBName   = flag.String("oar-postgres-dbname", "oar", "OAR Postresql database name.")
	oarInstance         = flag.String("oar-instance", "default", "OAR instance name.")
)

var (
	runningJobs = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "running_jobs",
			Help:      "OAR running jobs gauge",
		},
		[]string{
			"oar_instance",
		})
	waitingJobs = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "waiting_jobs",
			Help:      "OAR waiting jobs gauge",
		},
		[]string{
			"oar_instance",
		})
	finishingJobs = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "finishing_jobs",
			Help:      "OAR finishing jobs gauge",
		},
		[]string{
			"oar_instance",
		})
	queues = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "queues_state",
			Help:      "OAR queues status",
		},
		[]string{
			"oar_instance",
			"name",
		})
	runningJobsByQueues = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "running_jobs_by_queues",
			Help:      "OAR running jobs by queues",
		},
		[]string{
			"oar_instance",
			"queue",
		})
	waitingJobsByQueues = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "waiting_jobs_by_queues",
			Help:      "OAR waiting jobs by queues",
		},
		[]string{
			"oar_instance",
			"queue",
		})
	resourcesState = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "resources_state",
			Help:      "OAR resources state",
		},
		[]string{
			"oar_instance",
			"state",
		})
	suspectedNodes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "oar_exporter",
			Name:      "suspected_nodes",
			Help:      "OAR suspected nodes without jobs",
		},
		[]string{
			"oar_instance",
		})
)

func init() {
	prometheus.MustRegister(version.NewCollector("oar_exporter"))
	prometheus.MustRegister(runningJobs)
	prometheus.MustRegister(waitingJobs)
	prometheus.MustRegister(finishingJobs)
	prometheus.MustRegister(queues)
	prometheus.MustRegister(runningJobsByQueues)
	prometheus.MustRegister(waitingJobsByQueues)
	prometheus.MustRegister(resourcesState)
	prometheus.MustRegister(suspectedNodes)
}

func main() {
	flag.Parse()

	var oardb = oardatabase.OARDatabase{
		Hostname: *oarPostgresHostname,
		Port:     *oarPostgresPort,
		User:     *oarPostgresUser,
		Password: *oarPostgresPassword,
		DBName:   *oarPostgresDBName,
	}

	// OAR Database connection
	connected := false
	for !connected {
		err := oardb.InitConnection()
		if err != nil {
			fmt.Printf("Error connecting to postgresql server : %s\n", err)
			time.Sleep(10 * time.Second)
		} else {
			fmt.Println("Connected to postgresql database.")
			connected = true
		}
	}

	defer oardb.CloseDatabase()

	go func() {
		for {
			counts, err := oardb.GetJobsCounts()
			if err != nil {
				fmt.Printf("Error getting jobs counts : %s\n", err)
			} else {
				runningJobs.WithLabelValues(*oarInstance).Set(float64(counts["running"]))
				waitingJobs.WithLabelValues(*oarInstance).Set(float64(counts["waiting"]))
				finishingJobs.WithLabelValues(*oarInstance).Set(float64(counts["finishing"]))
			}

			qs, err := oardb.GetQueues()
			if err != nil {
				fmt.Printf("Error getting oar queues : %s\n", err)
			} else {
				for _, q := range qs {
					if q.State == "Active" {
						queues.WithLabelValues(*oarInstance, q.Name).Set(1)
					} else {
						queues.WithLabelValues(*oarInstance, q.Name).Set(0)
					}
				}
			}

			counts, err = oardb.GetResourceCountByState()
			if err != nil {
				fmt.Printf("Error getting resources counts by state : %s\n", err)
			} else {
				for k, v := range counts {
					resourcesState.WithLabelValues(*oarInstance, k).Set(float64(v))
				}
			}

			var stats = make(map[string]int32)

			counts, err = oardb.GetRunningJobsByQueues()
			if err != nil {
				fmt.Printf("Error getting running jobs by queues : %s\n", err)
			} else {
				for k, v := range counts {
					stats[k] = v
				}
				for _, q := range qs {
					if _, ok := stats[q.Name]; !ok {
						stats[q.Name] = 0
					}
				}
				for k, v := range stats {
					runningJobsByQueues.WithLabelValues(*oarInstance, k).Set(float64(v))
				}

				stats = make(map[string]int32)
				counts, err = oardb.GetWaitingJobsByQueues()
				if err != nil {
					fmt.Printf("Error getting waiting jobs by queues : %s\n", err)
				} else {
					for k, v := range counts {
						stats[k] = v
					}
					for _, q := range qs {
						if _, ok := stats[q.Name]; !ok {
							stats[q.Name] = 0
						}
					}
					for k, v := range stats {
						waitingJobsByQueues.WithLabelValues(*oarInstance, k).Set(float64(v))
					}
				}
			}

			suspectedNodesCount, err := oardb.GetSuspectedNodes()
			if err != nil {
				fmt.Printf("Error getting suspected nodes : %s\n", err)
			} else {
				suspectedNodes.WithLabelValues(*oarInstance).Set(float64(suspectedNodesCount))
			}

			time.Sleep(30 * time.Second)
		}
	}()

	// Start prometheus exporter HTTP server
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(*exporterAddr, nil))
}
